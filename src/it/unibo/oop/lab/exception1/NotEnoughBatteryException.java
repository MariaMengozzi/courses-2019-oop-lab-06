package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double batteryLevel;
    private final double batteyRequired;
    
	public NotEnoughBatteryException(final double batteryLevel, final double batteyRequired) {
		super();
		this.batteryLevel = batteryLevel;
		this.batteyRequired = batteyRequired;
	}

	@Override
	public String toString() {
		return "No enough battery for moving. Battery level is " + batteryLevel + " battery required is " + batteyRequired;
	}
	
	public String getMessage() {
        return this.toString();
    }
}
