package it.unibo.oop.lab.exception2;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;


/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	final AccountHolder a1 = new AccountHolder("Mario","Rossi", 1);
    	final BankAccount b1 = new StrictBankAccount(1,1000,10);
    	
    	final AccountHolder a2 = new AccountHolder("Luca","Bianchi", 2);
    	final BankAccount b2 = new StrictBankAccount(2,1000,10);
    	
    	try {
    		b1.deposit(3, 50);
    		
    	}catch(WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	
    	for (int i = 0; i < 10; i++) {
            try {
                b2.depositFromATM(a2.getUserID(), 1);
            } catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
            	fail("Not exceed number of transation"); ///
            }
        }
    	
    	/*
         * Questa istruzione genererà una eccezione di tipo
         * TransactionsOverQuotaException
         */
        try {
            b2.depositFromATM(a2.getUserID(), 1);
            fail("Should raise the exception signaling that we exceeded max no. transactions!");
        } catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
            assertNotNull(e);
        }
        try {
            /*
             * Questa istruzione genererà una eccezione di tipo
             * NotEnoughFoundsException
             */
            b1.withdraw(a1.getUserID(), 50_000);
        } catch (WrongAccountHolderException e) {
            fail();
        } catch (NotEnoughFoundsException e) {
            assertNotNull(e);
        }
    }
}
