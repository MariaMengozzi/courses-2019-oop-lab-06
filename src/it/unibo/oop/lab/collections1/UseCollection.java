package it.unibo.oop.lab.collections1;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

	private static final int ELEMS = 100_000;
    private static final int TO_MS = 1_000_000;
    
    

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	final List<Integer> arrayList = new ArrayList<>();
    	for (int i = 1000; i < 2000; i++) {
    		arrayList.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List<Integer> linkedList = new LinkedList<>(arrayList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	final int tmp = arrayList.get(arrayList.size()-1);
    	arrayList.set(arrayList.size()-1, arrayList.get(0));
    	arrayList.set(0, tmp);
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for( int i : arrayList) {
    		System.out.print(i + " ");
    	}
    	System.out.println();
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	long time = System.nanoTime();
    	for (int i = 1000; i < ELEMS; i++) {
    		arrayList.add(0,i);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("inserting 100.000 in a ArrayList took " + time
                + "ns (" + TimeUnit.NANOSECONDS.toMillis(time) + "ms)");
    	
    	time = System.nanoTime();
    	for (int i = 1000; i < ELEMS; i++) {
    		linkedList.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("inserting 100.000 in a LinkedList took " + time
                + "ns (" + TimeUnit.NANOSECONDS.toMillis(time) + "ms)");
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	int reading = 1000;
    	time = System.nanoTime();
    	while (reading != 0) {
    		arrayList.get((arrayList.size()-1) /2);
    		reading--;
    	}
    	time = System.nanoTime() - time;
    	System.out.println("reading 1000 times in a ArrayList the middle element took " 
    				+ time+ "ns (" + TimeUnit.NANOSECONDS.toMillis(time) + "ms)");
    	
    	reading = 1000;
    	time = System.nanoTime();
    	while (reading != 0) {
    		linkedList.get((linkedList.size()-1) /2);
    		reading--;
    	}
    	time = System.nanoTime() - time;
    	System.out.println("reading 1000 times in a LinkedList the middle element took " 
    				+ time+ "ns (" + TimeUnit.NANOSECONDS.toMillis(time) + "ms)");
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	
    	final long africa = 1_110_635_000L;
        final long americas = 972_005_000L;
        final long antarctica = 0L;
        final long asia = 4_298_723_000L;
        final long europe = 742_452_000L;
        final long oceania = 38_304_300L;
        
    	Map<String,Long> countryPopulation = new HashMap<>();
		countryPopulation.put("Africa", africa);
		countryPopulation.put("Americas", americas);
		countryPopulation.put("Antarctica", antarctica);
		countryPopulation.put("Asia", asia);
		countryPopulation.put("Europe", europe);
		countryPopulation.put("Oceania", oceania);
    		
    	
        /*
         * 8) Compute the population of the world
         */
		long populationOfTheWorld = 0L;
		for (String country : countryPopulation.keySet()) {
			populationOfTheWorld = populationOfTheWorld + countryPopulation.get(country);
		}
		System.out.println("The population of the world is: " + populationOfTheWorld);
    }
}
